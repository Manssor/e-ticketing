/**
 * View Models used by Spring MVC REST controllers.
 */
package com.elm.eticketing.web.rest.vm;
