import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { EticketingSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [EticketingSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [EticketingSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EticketingSharedModule {
  static forRoot() {
    return {
      ngModule: EticketingSharedModule
    };
  }
}
